package layout;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.TextFlow;
import server.Server;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Timo on 18.12.2015.
 */
public class Console {
    /************************************************************
     *                                                          *
     *                      @FXML objects                       *
     *                                                          *
     ************************************************************/

    @FXML AnchorPane scenePane;
    @FXML public TextArea show;
    @FXML public TextField in;
    @FXML public ListView userList;

    private Server server;

    /************************************************************/


    /**
     * runs right after the Scene is completely build
     */
    public void initialize() {
        in.requestFocus();
    }


    /**
     *
     * @param line
     */
    public void writeLine(String line) {
        if(show.getText().isEmpty()) show.setText(getDateTime() + line);
        else show.setText(show.getText() + "\n"+ getDateTime() + line);
    }
    /************************************************************/






    /************************************************************
     *                                                          *
     *                   optional methods                       *
     *                                                          *
     ************************************************************/

    /**
     *
     * @return dateTime
     */
    private String getDateTime(){
        DateFormat dateFormat = new SimpleDateFormat("[HH:mm:ss yyyy/MM/dd]   ");
        Calendar cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime()); //2014/08/06 16:00:22
    }

    /**
     *
     * @param server
     */
    public void initServer(Server server){
        this.server = server;
    }

    /**
     *
     * @return
     */
    public ListView getUserListView(){
        return userList;
    }

    /************************************************************/

}
