import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import layout.Console;
import layout.UserInterface;
import server.Server;

/**
 * Created by Timo on 14.12.2015.
 */
public class Start extends Application {

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    private Server server;
    private UserInterface userInterface;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    /**
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {

        // ===================================================
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout.fxml"));
        Parent root = loader.load();
        Console console = loader.<Console>getController();
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
        primaryStage.setOnCloseRequest(we -> {
            if (server != null) server.setListening(false);
            primaryStage.close();
            System.exit(0);
        });
        // ===================================================


        // ===================================================
        Task<Void> task = new Task<Void>() {
            @Override
            public Void call() {
                userInterface = new UserInterface();
                server = new Server(userInterface);
                server.setConsole(console);
                server.run();
                return null;
            }
        };
        new Thread(task).start();
        // ===================================================


    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }
}
