package server;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import jdk.nashorn.internal.objects.annotations.Getter;
import jdk.nashorn.internal.objects.annotations.Setter;
import layout.Console;
import layout.UserInterface;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Timo on 14.12.2015.
 */
public class Server implements Runnable {

    /**
     * ---------------------------------- declarating server objects ---------------------------------
     */
    private static final int PORT_NUM = 4000;
    private ObservableList clientList = FXCollections.observableArrayList ();
    /**
     * ---------------------------------- declarating io objects -------------------------------------
     */
    private ServerSocket serverSocket;
    private Socket socket;
    private boolean running = true;
    private UserInterface userInterface;
    private Console console;
    /** -------------------------------------------------------------------------------------------- */


    /**
     *
     */
    public Server(UserInterface userInterface) {
        // =====================================
        this.userInterface = userInterface;
        // =====================================
        try {
            // ============================================
            initServer();


            // ============================================
        } catch (IOException e) {
            e.printStackTrace();
            console.writeLine("ERROR 100: IO EXCEPTION");
        }
    }

    /**
     *
     */
    public void run() {
        try {
            // ============================================
            listening();

            // ============================================
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                serverSocket.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @throws IOException
     */
    private void initServer() throws IOException {
        serverSocket = new ServerSocket(PORT_NUM);
    }


    /**
     * loops our server. If running is true, the server is listening to our socket and accepting new Clients
     *
     * @throws IOException
     */
    private void listening() throws IOException {
        console.writeLine("SERVER LISTENING NOW ON: " + PORT_NUM);
        while (running) {
            // ==================================================================
            socket = serverSocket.accept();
            ServerThread serverThread = new ServerThread(socket, clientList, console);
            clientList.add(serverThread);
            serverThread.start();
            // ==================================================================
        }
    }


    /*********************************************************************
     *                                                                   *
     *                     GETTER & SETTER AREA                          *
     *                                                                   *
     * *******************************************************************
     */

    @Getter
    /**
     * @return boolean if the server is running actually
     */
    public boolean isListening() {
        return running;
    }

    @Setter
    /**
     *
     * @param running
     */
    public void setListening(boolean running) {
        this.running = running;
    }

    @Setter
    /**
     *
     */
    public void setConsole(Console console) {
        this.console = console;
        initUserList();
    }


    /**
     *
     */
    private void initUserList() {
        console.getUserListView().setItems(clientList);
    }

}
