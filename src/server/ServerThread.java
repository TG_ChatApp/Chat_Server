package server;

import javafx.collections.ObservableList;
import layout.*;
import layout.Console;

import java.io.*;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.RunnableFuture;

/**
 * Created by Timo on 14.12.2015.
 */
public class ServerThread extends Thread {

    // ------------------------------------------ declerating our vars --------------------------------------
    private Socket socket;
    private BufferedReader bufferedReader;
    private BufferedWriter bufferedWriter;
    private ObservableList clientList;
    private layout.Console console;
    // ------------------------------------------------------------------------------------------------------

    /**
     * @param socket
     * @param clientList
     */
    public ServerThread(Socket socket, ObservableList clientList, Console console) {
        this.socket = socket;
        this.clientList = clientList;
        this.console = console;
    }

    /**
     *
     */
    public void run() {
        try {
            console.writeLine("NEW SERVERTHREAD STARTED:");
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

            console.writeLine(getMessage());

        } catch (IOException e) {
           console.writeLine("ERROR 1000: IO EXCEPTION: SERVER THREAD");
        } finally {
            try {
                console.writeLine("SERVERTHREAD CLOSED!");
                clientList.remove(this);
                bufferedWriter.close();
                bufferedWriter.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
                console.writeLine("SERVERTHREAD COULD NOT BE CLOSED");
            }
        }
    }


    /********************************************************
     *                                                      *
     *                  messaging methods                   *
     *                                                      *
     ********************************************************/

    /**
     * @return inputMessage
     * @throws IOException
     */
    public String getMessage() throws IOException {
        String inputMessage = bufferedReader.readLine();
        return inputMessage;
    }

    /**
     *
     * @param outputMessage
     * @throws IOException
     */
    public void sendMessage(String outputMessage) throws IOException {
        bufferedWriter.write(outputMessage + "\n");
        bufferedWriter.flush();
    }

    /********************************************************
     *                                                      *
     *                   running methods                    *
     *                                                      *
     ********************************************************/

    /**
     *
     * @throws IOException
     */
    private void loopForResponse() throws IOException{
        while(bufferedReader.readLine() != null){

        }
    }

    /********************************************************/
}
